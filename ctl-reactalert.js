
var React = require('react');
var ReactBootstrap = require('react-bootstrap');

var AutoAlert = React.createClass({
  getDefaultProps: function() {
    return {
      secondsToDisplay: 4
    };
  },

  getInitialState: function() {
    return {
      alertVisible: true
    };
  },

  render: function() {

    var Alert = ReactBootstrap.Alert;
    if (this.state.alertVisible) {
      return (
      React.createElement(Alert, {
        style: {
          borderRadius: 'initial',
          border: 'initial'
        },
        bsStyle: this.props.bsStyle || "danger",
        onDismiss: this.handleAlertDismiss,
        dismissAfter: this.props.secondsToDisplay * 1000
      },
        React.createElement("h4", null, this.props.title),
        React.createElement("p", null, this.props.msg)
      )
      );
    } else {
      return (null);
    }
  },

  handleAlertDismiss: function() {

    this.setState({
      alertVisible: false
    });

    if (this.props.onDismiss) {
      this.props.onDismiss();
    }
  },

});

var reactAlert = function(title, msg, secondsToDisplay, onDismiss, style) {

  //clear hidden currentNode if exists
  React.unmountComponentAtNode(document.getElementById('alertNode'));

  var Alert = AutoAlert;

  var alertInstance = (
  React.createElement(Alert, {
    title: title,
    msg: msg,
    secondsToDisplay: secondsToDisplay,
    onDismiss: onDismiss,
    bsStyle: style
  }
  )
  );

  React.render(alertInstance, document.getElementById('alertNode'));

};

module.exports = reactAlert;
