import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';


var LabelTip = React.createClass({

  shouldComponentUpdate: function(nextProps, nextState) {
    return (nextProps.tip !== this.props.tip);
  },

  render: function() {

    var OverlayTrigger = ReactBootstrap.OverlayTrigger;
    var Tooltip = ReactBootstrap.Tooltip;
    var Glyphicon = ReactBootstrap.Glyphicon;
    var Popover = ReactBootstrap.Popover;
    var Button = ReactBootstrap.Button;


    const tooltip = (
    <OverlayTrigger trigger={['hover', 'click']} placement='right' overlay={<Tooltip><strong>{this.props.tip}</strong></Tooltip>}>
      			<Glyphicon glyph="question-sign" />
    		</OverlayTrigger>
    );

    return (
      <div className={this.props.className}>
        		<span style={{
        marginRight: "5px"
      }}>{this.props.label}</span>
	        	<span onClick={function(e) {
        e.preventDefault();
        e.stopPropagation();
      }}>
	        	{ tooltip }
	        	</span>
            </div>
      );
  }
});

export default LabelTip;
