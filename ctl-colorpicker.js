﻿import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import Formsy from 'formsy';
import global from 'global';

var ColorPickerInt = React.createClass({

    mixins: [Formsy.Mixin],

    onPick: function (evt) {

        function componentToHex(c) {
            var hex = c.toString(16);
            return hex.length == 1 ? "0" + hex : hex;
        }

        function rgbToHex(r, g, b) {
            return componentToHex(r) + componentToHex(g) + componentToHex(b);
        }

        var hex;
        eval('hex = rgbToHex(' + evt.target.style.backgroundColor.substr(4)
            .replace(')', '') + ');');

        React.findDOMNode(this.refs.Sample)
            .style.backgroundColor = hex;
        this.setValue(hex);
    },

    onColourChangedByHand: function (evt) {
        React.findDOMNode(this.refs.Sample)
            .style.backgroundColor = evt.currentTarget.value;
        this.setValue(evt.currentTarget.value);
    },

    shouldComponentUpdate: function (nextProps, nextState) {

        return  (nextState._value !== this.state._value) ||
                (nextProps.value !== this.props.value) ||
                (nextState._isValid !== this.state._isValid) ||
                (nextProps.placeholder !== this.props.placeholder) ||
                (nextState._isRequired !== this.state._isRequired) ||
                (nextProps.disabled !== this.props.disabled) ||
                (nextProps.label !== this.props.label);
    },

    render: function () {

        var OverlayTrigger = ReactBootstrap.OverlayTrigger;
        var Popover = ReactBootstrap.Popover;
        var Row = ReactBootstrap.Row;
        var Col = ReactBootstrap.Col;
        var Button = ReactBootstrap.Button;

        var picker = < div style = {
                {
                    textAlign: 'center'
                }
            } >
            <span className="palette" title="Purples">
        <span className="swatch" onClick={this.onPick} style={{ backgroundColor: "rgb(252, 251, 253)"}}></span> < span className =
            "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(239, 237, 245)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(218, 218, 235)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(188, 189, 220)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(158, 154, 200)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(128, 125, 186)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(106, 81, 163)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(84, 39, 143)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(63, 0, 125)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Blues" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 251, 255)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(222, 235, 247)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(198, 219, 239)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(158, 202, 225)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(107, 174, 214)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(66, 146, 198)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(33, 113, 181)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(8, 81, 156)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(8, 48, 107)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Greens" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 252, 245)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(229, 245, 224)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(199, 233, 192)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(161, 217, 155)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(116, 196, 118)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(65, 171, 93)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(35, 139, 69)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(0, 109, 44)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(0, 68, 27)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Oranges" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 245, 235)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 230, 206)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 208, 162)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 174, 107)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 141, 60)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(241, 105, 19)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(217, 72, 1)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(166, 54, 3)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(127, 39, 4)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Reds" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 245, 240)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 224, 210)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(252, 187, 161)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(252, 146, 114)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(251, 106, 74)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(239, 59, 44)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(203, 24, 29)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(165, 15, 21)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(103, 0, 13)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Greys" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 255)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(240, 240, 240)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(217, 217, 217)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(189, 189, 189)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(150, 150, 150)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(115, 115, 115)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(82, 82, 82)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(37, 37, 37)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
                {
                    backgroundColor: "rgb(0, 0, 0)"
                }
            } > < /span> < /span >

            < span className = "palette"
        title = "Yellow Green" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 229)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 252, 185)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(217, 240, 163)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(173, 221, 142)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(120, 198, 121)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(65, 171, 93)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(35, 132, 67)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(0, 104, 55)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(0, 69, 41)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Yellow Green Blue" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 217)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(237, 248, 177)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(199, 233, 180)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(127, 205, 187)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(65, 182, 196)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(29, 145, 192)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(34, 94, 168)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(37, 52, 148)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(8, 29, 88)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Green Blue" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 252, 240)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(224, 243, 219)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(204, 235, 197)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(168, 221, 181)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(123, 204, 196)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(78, 179, 211)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(43, 140, 190)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(8, 104, 172)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(8, 64, 129)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Blue Green" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 252, 253)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(229, 245, 249)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(204, 236, 230)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(153, 216, 201)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(102, 194, 164)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(65, 174, 118)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(35, 139, 69)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(0, 109, 44)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(0, 68, 27)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Purple Blue Green" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 247, 251)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(236, 226, 240)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(208, 209, 230)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(166, 189, 219)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(103, 169, 207)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(54, 144, 192)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(2, 129, 138)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(1, 108, 89)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(1, 70, 54)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Purple Blue" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 247, 251)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(236, 231, 242)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(208, 209, 230)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(166, 189, 219)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(116, 169, 207)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(54, 144, 192)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(5, 112, 176)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(4, 90, 141)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(2, 56, 88)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Blue Purple" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 252, 253)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(224, 236, 244)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(191, 211, 230)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(158, 188, 218)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(140, 150, 198)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(140, 107, 177)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(136, 65, 157)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(129, 15, 124)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(77, 0, 75)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Red Purple" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 247, 243)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 224, 221)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(252, 197, 192)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(250, 159, 181)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 104, 161)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(221, 52, 151)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(174, 1, 126)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(122, 1, 119)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(73, 0, 106)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Purple Red" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 244, 249)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(231, 225, 239)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(212, 185, 218)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(201, 148, 199)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(223, 101, 176)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(231, 41, 138)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(206, 18, 86)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(152, 0, 67)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(103, 0, 31)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Orange Red" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 247, 236)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 232, 200)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 212, 158)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 187, 132)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(252, 141, 89)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(239, 101, 72)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(215, 48, 31)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(179, 0, 0)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(127, 0, 0)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Yellow Orange Red" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 204)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 237, 160)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 217, 118)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 178, 76)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 141, 60)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(252, 78, 42)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(227, 26, 28)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(189, 0, 38)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(128, 0, 38)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Yellow Orange Blue" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 229)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 247, 188)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 227, 145)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 196, 79)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 153, 41)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(236, 112, 20)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(204, 76, 2)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(153, 52, 4)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
                {
                    backgroundColor: "rgb(102, 37, 6)"
                }
            } > < /span> < /span >

            < span className = "palette"
        title = "Accent" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(127, 201, 127)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(190, 174, 212)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 192, 134)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 153)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(56, 108, 176)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(240, 2, 127)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(191, 91, 23)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(102, 102, 102)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Dark" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(27, 158, 119)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(217, 95, 2)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(117, 112, 179)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(231, 41, 138)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(102, 166, 30)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(230, 171, 2)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(166, 118, 29)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(102, 102, 102)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Pastel" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(251, 180, 174)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(179, 205, 227)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(204, 235, 197)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(222, 203, 228)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(254, 217, 166)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 204)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(229, 216, 189)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 218, 236)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Pastel" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(179, 226, 205)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(253, 205, 172)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(203, 213, 232)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(244, 202, 228)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(230, 245, 201)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 242, 174)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(241, 226, 204)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(204, 204, 204)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Set1" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(55, 126, 184)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(77, 175, 74)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(152, 78, 163)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 127, 0)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 255, 51)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(166, 86, 40)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(247, 129, 191)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(153, 153, 153)"
            }
        } > < /span> < /span > < span className = "palette"
        title = "Set2" >
            < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(102, 194, 165)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(252, 141, 98)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(141, 160, 203)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(231, 138, 195)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(166, 216, 84)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(255, 217, 47)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(229, 196, 148)"
            }
        } > < /span> < span className = "swatch"
        onClick = {
            this.onPick
        }
        style = {
            {
                backgroundColor: "rgb(179, 179, 179)"
            }
        } > < /span> < /span > < /div>;

        var errorMessage = this.showRequired() ? "Required" : !this.isValid() ? (this.getErrorMessage() || "") : "";

        var className = this.isPristine() ? null : this.showRequired() ? 'error' : this.showError() ? 'error' : 'success';

        var Input = ReactBootstrap.Input;

        return (
            <Row>
            <Col xs={6} >
            <Input
                {...this.props}
                required = {false} //.. disable html5 val
                type='text'
                ref='colorInput'
                help={errorMessage}
                bsStyle={className}
                value= { this.getValue() }
                addonBefore = "#"
                onChange = {this.onColourChangedByHand}
                ></Input>
            </Col>
            <Col xs={6} style= { { verticalAlign:"bottom" }} >
            {!this.props.disabled ?
            <OverlayTrigger trigger='click' rootClose = {true} placement = 'top' overlay = {<Popover key={Math.random()} title='Color Choices' id='popovercolorpicker'>
               {picker} </Popover>
            }>
                <Button ref="Sample" style={{ backgroundColor: '#'+this.getValue(), color: "black", marginTop:"25px" }}>Pick Colour</Button>
            </OverlayTrigger>
            : null }
            </Col>
            </Row>
        );
    },

});

var ColorPicker = React.createClass({

    render: function () {
        return <ColorPickerInt {
            ...this.props
        }
        validations = "isHexColour"
        validationError = "Value must be a valid HTML Hex Colour" / > ;
    }

});

export default ColorPicker;
