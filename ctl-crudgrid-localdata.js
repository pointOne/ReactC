
import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import global from 'global';
import CrudControls from 'ctl-crudgridcontrols';

var CrudGridReferences = React.createClass({

  mixins: [Formsy.Mixin],
  renderControlsIntoControlColumn: function() {
    var _entityNameCamel = this.props.entityNameCamelCase;
    var _entityNamePascal = this.props.entityNamePascalCase;
    let nodeTitle = `controlContainer${_entityNameCamel}`;
    $(`[id^='${nodeTitle}']`).each(function(i, e) {

      let container = document.getElementById(e.id);
      let o = _.find(this.props.data, {
        Id: e.id.substring(nodeTitle.length)
      });
      React.render(<CrudControls
      dataObject={o}
      entityNameCamelCase={_entityNameCamel}
      entityNamePascalCase={_entityNamePascal}
      dataLocal={true}
      />, container);
    }.bind(this));
  },

  loadGrid: function() {

    var _entityNamePascal = this.props.entityNamePascalCase;
    var _entityNameCamel = this.props.entityNameCamelCase;
    var _tableId = "#table" + _entityNamePascal;
    var _nameColumn = this.props.nameColumn;

    var cols;
    if (this.props.column2) {
      cols = [{
        "data": "Id"
      }, {
        "data": "Created"
      }, {
        "data": _nameColumn
      }, {
        "data": this.props.column2.field
      },
        null //controls
      ];
    } else {
      cols = [{
        "data": "Id"
      }, {
        "data": "Created"
      }, {
        "data": _nameColumn
      },
        null //controls
      ];
    }
    var colNo = (this.props.column2) ? 4 : 3;

    $(_tableId)
      .dataTable({
        "data": this.props.data,
        "autoWidth": false,
        filter: false,
        lengthChange: false,
        "initComplete": function(settings, json) {
          this.renderControlsIntoControlColumn();
        }.bind(this),
        paging: false,
        info: false,
        "destroy": true, //useful if rendered twice
        "order": [
          [1, "asc"]
        ],
        "columns": cols,
        "columnDefs": [{
          "targets": [0],
          "visible": false,
          "searchable": false,

        }, {
          "targets": [1],
          "visible": false,
          "searchable": false,

        }, {
          "targets": colNo,
          "data": null,
          "width": "125",
          "render": function(o) {
            let container = document.getElementById(`controlContainer${_entityNameCamel}${o.Id}`);
            if (container) {
              React.unmountComponentAtNode(container);
            }
            return `<div id="controlContainer${_entityNameCamel}${o.Id}"></div>`;
          }
        }]
      }).on('draw.dt', function() {
      this.renderControlsIntoControlColumn();
    }.bind(this));

  },

  componentDidMount: function() {
    this.loadGrid();
  },

  componentDidUpdate: function(prevProps, prevState) {
    this.loadGrid();
  },

  subs: [],

  componentWillMount: function() {

    var _entityNameCamel = this.props.entityNameCamelCase;

    var unMount = function(data, envelope) {
      $("[id^='controlContainer']").each(function(i, e) {
        let container = document.getElementById(e.id);
        React.unmountComponentAtNode(container);
      });
    };

    this.subs.push(global.sub(this.props.entityNameCamelCase, "grid-requested-add", unMount));
    this.subs.push(global.sub(this.props.entityNameCamelCase, "grid-requested-edit", unMount));
    this.subs.push(global.sub(this.props.entityNameCamelCase, "removing", (data, env) => {
      var node = document.getElementById(`controlContainer${_entityNameCamel}${data}`);
      global.pub(this.props.entityNameCamelCase, 'removed', data);
      setTimeout(() => React.unmountComponentAtNode(node), 500);
    }));
  },

  componentWillUnmount: function() {
    $.each(this.subs, function(index, value) {
      value.unsubscribe();
    });

    $("[id^='controlContainer']").each(function(i, e) {
      let container = document.getElementById(e.id);
      React.unmountComponentAtNode(container);
    });
  },

  render: function() {

    var panelHeader = this.props.panelHeader;
    var _entityNameCamel = this.props.entityNameCamelCase;
    var tableId = "table" + this.props.entityNamePascalCase;
    var tableStyle = {

    };
    var nameColumnHeader = this.props.nameColumnHeader || "Name";

    var Panel = ReactBootstrap.Panel;
    var Button = ReactBootstrap.Button;

    var column2;
    if (this.props.column2) {
        column2 = <th> { this.props.column2.header } </th>;
    }
    var errorMessage = this.getErrorMessage() || "";
    return (
      <Panel header={panelHeader} ref="grid">
                    <table className="table table-bordered table-striped table-hover" id={tableId} style={tableStyle}>
                        <thead>
                            <tr>
                                <th className="hide">Id</th>
                                <th className="hide">Created</th>
                                <th>{nameColumnHeader}</th>
                                {column2}
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <Button bsStyle="default" onClick={ () => global.pub(_entityNameCamel, 'grid-requested-add') }>Add</Button>
                    <div className="has-error"><span className="help-block">{errorMessage}</span></div>                    
                </Panel>
      );
  }
});

export default CrudGridReferences;
