import React from 'react';
import Formsy from 'formsy';
import * as ReactBootstrap from 'react-bootstrap';

export var Radio = React.createClass({

  render: function() {

    var Input = ReactBootstrap.Input;

    return (
      <div style={{
        marginLeft: '10px'
      }}>
      <Input
      {...this.props}
      type="radio"
      label={this.props.label}
      value={this.props.value}
      checked={this.props.checked}
      onChange={(e) => {
        if (this.props.onChange) {
          this.props.onChange(this.props.value);
        }
      }} /></div>);
  }
});

export default React.createClass({

  mixins: [Formsy.Mixin],

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  //too hard and not worth it to calculate if the disabled property of child radio's has changed
  },

  changeValue: function(newValue) {

    this.setValue(newValue);
    if (this.props.onChange) {
      this.props.onChange(newValue);
    }
  },

  render: function() {
    return (
      <div style={{
        display: 'flex',
        border: '1px solid #cccccc',
        borderRadius: '4px',
        paddingTop: '5px',
        margin: '5px 0px',
        justifyContent: this.props.justifyContent || 'flex-start'
      }}>
        {React.Children.map(this.props.children, child => {
        return React.cloneElement(child, _.assign({
          checked: child.props.value === this.getValue(),
          onChange: this.changeValue,
          disabled: this.props.disabled
        }, child.props));
      })}
    </div>
      );
  }
});
