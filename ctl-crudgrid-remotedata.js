import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import global from 'global';
import store from 'store';
import CrudControls from 'ctl-crudgridcontrols';

var CrudGrid = React.createClass({

  renderControlsIntoControlColumn: function() {

    var _entityNameCamel = this.props.entityNameCamelCase;
    var _entityNamePascal = this.props.entityNamePascalCase;
    var _deleteWarning = this.props.deleteWarning;
    let nodeTitle = `controlContainer${_entityNameCamel}`;
    $(`[id^='${nodeTitle}']`).each(function(i, e) {
      let container = document.getElementById(e.id);
      let o = _.find(store['get' + _entityNamePascal + 'Collection'](), {
        Id: e.id.substring(nodeTitle.length)
      });

      React.render(
        <CrudControls dataObject={o}
        deleteWarning={_deleteWarning}
        entityNameCamelCase={_entityNameCamel}
        entityNamePascalCase={_entityNamePascal}
        />, container);
    });
  },

  renderCustomNameColumn: function() {
    var _nameColumn = this.props.nameColumn;
    var _entityNameCamel = this.props.entityNameCamelCase;
    var _entityNamePascal = this.props.entityNamePascalCase;
    var _children = this.props.children;
    let nodeTitle = `customContainer${_entityNameCamel}`;
    $(`[id^='${nodeTitle}']`).each(function(i, e) {
      let container = document.getElementById(e.id);
      let o = _.find(store['get' + _entityNamePascal + 'Collection'](), {
        Id: e.id.substring(nodeTitle.length)
      });
      var newProps = {
        data: o
      };
      _.merge(newProps, this.props);
      React.render(
        <div><span>{o[_nameColumn]}</span><div style={{
          float: 'right'
        }}>
        {React.Children.map(_children, (child) => React.cloneElement(child, newProps))}
        </div></div>, container);
    });
  },


  loadGrid: function(omitLoader, reload) {

    var _entityNamePascal = this.props.entityNamePascalCase;
    var _entityNameCamel = this.props.entityNameCamelCase;
    var _nameColumn = this.props.nameColumn;
    var _renderControls = this.renderControlsIntoControlColumn;
    var _renderCustomNameColumn = this.renderCustomNameColumn;
    var cols = [{
      "data": "Id"
    }, {
      "data": "Created"
    }, {
      "data": this.props.children ? null : _nameColumn
    }];
    if (this.props.column2) {
      cols[cols.length] = {
        "data": this.props.column2.field
      };
    }
    cols[cols.length] = null; //control column

    var controlColumnNo = cols.length - 1;

    store['load' + _entityNamePascal + 'Collection'](omitLoader, reload)
      .done(function(data) {
        $("#table" + this.props.entityNamePascalCase)
          .DataTable({
            "data": data,
            "deferRender": true,
            "initComplete": function(settings, json) {
              _renderControls();
              _renderCustomNameColumn();
            },
            "autoWidth": false,
            "destroy": true,
            language: {
              searchPlaceholder: "Across All Columns"
            },
            "order": [
              [1, "asc"]
            ],
            "columns": cols,
            "columnDefs": [{
              "targets": 0,
              "visible": false,
              "searchable": false,
            }, {
              "targets": 1,
              "visible": false,
              "searchable": false,
            },
              {
                "targets": 2,
                "data": null,
                "render": this.props.children ? function(o) {
                  let container = document.getElementById(`customContainer${_entityNameCamel}${o.Id}`);
                  if (container) {
                    React.unmountComponentAtNode(container);
                  }
                  return `<div id="customContainer${_entityNameCamel}${o.Id}"></div>`;
                } : undefined
              },
              {
                "targets": controlColumnNo,
                "data": null,
                "width": "70",
                "render": function(o) {
                  let container = document.getElementById(`controlContainer${_entityNameCamel}${o.Id}`);
                  if (container) {
                    React.unmountComponentAtNode(container);
                  }
                  return `<div id="controlContainer${_entityNameCamel}${o.Id}"></div>`;
                }
              }]
          })
          .on('draw.dt', function() {
            //rendering controls here means we only render them for the current page
            _renderControls();
            _renderCustomNameColumn();
          });

        var columnSearchSettings = [{
          column_number: 2,
          filter_type: "text",
          filter_default_label: ''
        }];
        if (this.props.column2) {
          var column2SearchSettings = {
            column_number: 3,
            filter_default_label: ''
          }; //3 is 2 to callers
          if (this.props.column2.searchSettings) {
            _.assign(column2SearchSettings, this.props.column2.searchSettings);
          }
          columnSearchSettings.push(column2SearchSettings);
        }

        yadcf.init($("#table" + this.props.entityNamePascalCase).DataTable(), columnSearchSettings);

      }.bind(this));

  },


  componentDidMount: function() {
    this.loadGrid();
  },

  componentWillUpdate: function(x, y) {
    this.loadGrid();
  },

  subs: [],

  componentWillMount: function() {

    var _entityNameCamel = this.props.entityNameCamelCase;

    var unMount = function(data, envelope) {
      $("[id^='controlContainer']").each(function(i, e) {
        let container = document.getElementById(e.id);
        React.unmountComponentAtNode(container);
      });
      $("[id^='customContainer']").each(function(i, e) {
        let container = document.getElementById(e.id);
        React.unmountComponentAtNode(container);
      });
    };

    this.subs.push(global.sub(this.props.entityNameCamelCase, "grid-requested-add", unMount));
    this.subs.push(global.sub(this.props.entityNameCamelCase, "grid-requested-edit", unMount));
    this.subs.push(global.sub(this.props.entityNameCamelCase, "removing", (data, env) => {
      let node1 = document.getElementById(`controlContainer${_entityNameCamel}${data}`);
      let node2 = document.getElementById(`customContainer${_entityNameCamel}${data}`);
      store['remove' + this.props.entityNamePascalCase](data).done(() => {
        if (node1) {
          React.unmountComponentAtNode(node1);
        }
        if (node2) {
          React.unmountComponentAtNode(node2);
        }
        global.pub(this.props.entityNameCamelCase, 'removed', data);
      });
    }));
  },

  componentWillUnmount: function() {

    $.each(this.subs, function(index, value) {
      value.unsubscribe();
    }.bind(this));

    $("[id^='controlContainer']").each(function(i, e) {
      let container = document.getElementById(e.id);
      React.unmountComponentAtNode(container);
    });
    $("[id^='customContainer']").each(function(i, e) {
      let container = document.getElementById(e.id);
      React.unmountComponentAtNode(container);
    });
  },

  render: function() {
    var panelHeader = this.props.panelHeader;
    var _entityNameCamel = this.props.entityNameCamelCase;
    var tableId = "table" + this.props.entityNamePascalCase;

    var Panel = ReactBootstrap.Panel;
    var Button = ReactBootstrap.Button;
    var Glyphicon = ReactBootstrap.Glyphicon;

    var column2;
    if (this.props.column2) {
      column2 = <th> { this.props.column2.header } </th>;
    }

    return (
      <Panel header={<div>{panelHeader}<a style={{
        float: 'right',
        padding: '3px 6px 3px'
      }} onClick={() => this.loadGrid(false, true)}><Glyphicon id="refresh-grid-icon" glyph="repeat"></Glyphicon></a></div>} ref="grid">
        <table className="table table-bordered table-hover" id={tableId}>
          <thead>
            <tr>
              <th className="hide">Id</th>
              <th className="hide">Created</th>
              <th><span>Name</span></th>
              { column2 }
              <th></th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
        <Button bsStyle="default" onClick={ function() {
        global.pub(_entityNameCamel, 'grid-requested-add');
      }}>Add</Button>
        </Panel>
      );
  }
});

export default CrudGrid;
