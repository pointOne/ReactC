
var Formsy = require('formsy');

var isExisty = function(value) {
  return value !== null && value !== undefined;
};

var isEmpty = function(value) {
  return value === '';
};

Formsy.addValidationRule('isLessThan', function(values, value, lessThan) {
  return !isExisty(value) || isEmpty(value) || value < lessThan;
});
Formsy.addValidationRule('isLessThanOrEqualTo', function(values, value, lessThan) {
  return !isExisty(value) || isEmpty(value) || value <= lessThan;
});
Formsy.addValidationRule('isGreaterThan', function(values, value, greaterThan) {
  return !isExisty(value) || isEmpty(value) || value > greaterThan;
});
Formsy.addValidationRule('isGreaterThanOrEqualTo', function(values, value, greaterThan) {
  return !isExisty(value) || isEmpty(value) || value >= greaterThan;
});
Formsy.addValidationRule('isLessThanField', function(values, value, field) {
  return !isExisty(value) || isEmpty(value) || value < values[field];
});
Formsy.addValidationRule('isLessThanOrEqualToField', function(values, value, field) {
  return !isExisty(value) || isEmpty(value) || value <= values[field];
});
Formsy.addValidationRule('forced', function(values, value, field) {
  return true; //force validation rendering on a field which is only validated externally
});
Formsy.addValidationRule('isMoney', function(values, value) {
  if (!isExisty(value) || isEmpty(value)) {
    return true;
  } else if (typeof value === 'number') {
    return true;
  } else {
    var matchResults = value !== undefined && value.match(/^[-+]?\d{1,5}([.]\d{1,2})?$/);
    if (!!matchResults) {
      return matchResults[0] == value;
    } else {
      return false;
    }
  }
});
Formsy.addValidationRule('isRequiredIfChecked', function(values, value, field) {
  if (values[field]) {
    return Boolean(value);
  } else {
    return true;
  }
});
Formsy.addValidationRule('isNotRequiredIfChecked', function(values, value, field) {
  if (values[field]) { //tickbox ticked
    return true;
  } else {
    return Boolean(value);
  }
});
Formsy.addValidationRule('isRequiredIfFieldEquals', function(values, value, objFieldAndValue) {
  if (values[objFieldAndValue.field] === objFieldAndValue.value) { //tickbox ticked
    return Boolean(value);
  } else {
    return true;
  }
});
Formsy.addValidationRule('timeOfDayIsEarlierThan', function(values, value, field) {
  if (value && values[field]) {
    var timeAsNumber = Number(value.replace(/:/g, ''));
    var compareTo = Number(values[field].replace(/:/g, ''));
    return timeAsNumber < compareTo;
  } else {
    return true;
  }
});

Formsy.addValidationRule('isHexColour', function(values, value, field) {
  if (value) {
    return /^[0-9A-F]{6}$/i.test(value);
  } else {
    return true;
  }
});

Formsy.addValidationRule('isMultiSelectDefaultValue', function(values, value) {
  return (value === undefined || (Array.isArray(value) && value.length === 0));
});

Formsy.addValidationRule('isNotExactlyEqualTo', function(values, value, param) {
  return value !== param;
});

Formsy.addValidationRule('isExactlyEqualTo', function(values, value, param) {
  return value === param;
});
Formsy.addValidationRule('notEquals', function(values, value, param) {
  return value != param;
});

Formsy.addValidationRule('isNotCombinedWith', function(values, value, field) {
  return ((isEmpty(values[field]) || !isExisty(values[field])) ||
    (isEmpty(value) || !isExisty(value)));
});
Formsy.addValidationRule('stringDoesNotContain', function(values, value, param) {
  if (value === null || value === undefined) {return true;}
  return testable.indexOf(param) !== -1;
});
Formsy.addValidationRule('arrayDoesNotContain', function(values, value, param) {
  if (value === null || value === undefined) {return true;}
  return _.every(value, function(i) {
    return i !== param;
  });
});

//Formsy.addValidationRule('new', function(values, value, param) {
//    _onsole.log('param', param);
//    _onsole.log('value', value);
//    _onsole.log('values', values);
//    return true;
//});

/* override isDefaultEmpty */

Formsy.addValidationRule('isDefaultRequiredValue', function(values, value) {
  return value === undefined || value === '' || value === null || value === 0 || value === '00000000-0000-0000-0000-000000000000';
});
