(function() {
  'use strict';

  /**
   * Mixin for persistent storage.
   * @public
   */
  var ReactPersistentState = {

    /**
     * Sets unique identifier for the current component.
     */
    setPId: function(id) {
      this._pid = id;
    },

    /**
     * Sets storage.
     */
    setPStorage: function(storage) {
      this._pstorage = storage;
    },

    /**
     * Sets the component's state and save it
     * using the provided storage.
     */
    setPState: function(state) {
      var self = this;
      if (this._pid === undefined) {
        throw new Error('You must explicitly set the ' +
          'component\'s persistent id');
      }
      if (!this._pstorage || typeof this._pstorage.set !== 'function') {
        throw new Error('Set storage befor using setPState');
      }

      //save to storage
      self._pstorage.set(self._pid, _.cloneDeep(state));
    },

    /**
     * Removes the state from the persistent storage.
     */
    removePState: function() {
      if (this._pid === undefined) {
        throw new Error('You must explicitly set the ' +
          'component\'s persistent id');
      }
      return this._pstorage.remove(this._pid);
    },

    /**
     * Restores the component state based on its content in
     * the provided persistent storage.
     */
    restorePState: function(cb) {
      if (this._pid === undefined) {
        throw new Error('You must explicitly set the ' +
          'component\'s persistent id');
      }
      var self = this;
      return this._pstorage.get(this._pid)
        .then(function(data) {
          self.setState(data, cb);
          return data;
        });
    },

    /**
     * localStorage for persistent storage
     * @public
     */
    localStorage: {

      /**
       * Sets value associated with given id.
       * @param {string} id
       * @param {object} data
       * @return {Promise} Once the data has been stored the
       *  promise will be resolved otherwise rejected.
       */
      set: function(id, data) {
        try {
          localStorage.setItem(id, JSON.stringify(data));
        } catch (e) {
          return Promise.reject(e);
        }
        return Promise.resolve(data);
      },

      /**
       * Gets data from the storage based on given id.
       * @param {string} id
       * @return {Promise} The promise will be resolved with the
       *  data associated with the id
       */
      get: function(id) {
        var data;
        try {
          data = JSON.parse(localStorage.getItem(id) || '{}');
        } catch (e) {
          console.warn('Can\'t parse the current state.');
        }
        return Promise.resolve(data);
      },

      /**
       * Removes data associated to given id.
       * @param {string} id
       * @return {Promise}
       */
      remove: function(id) {
        localStorage.removeItem(id);
        return Promise.resolve(id);
      }
    }
  };

  module.exports = ReactPersistentState;

}());
