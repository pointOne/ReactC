import store from 'store';
import React from 'react';

function mixins() {

  this.onValid = function() {
    if (this.state.canSubmit === false) {
      this.setState({
        canSubmit: true
      });
    }
  };

  this.onInvalid = function() {
    if (this.state.canSubmit === true) {
      this.setState({
        canSubmit: false
      });
    }
  };

  this.getAsyncOptionsFromPath = function(path) {
    var asyncOptionsFunc = function(input, callback) {
      store.exec(path).done(function(data) {
        callback(null, {
          options: data,
          complete: true
        });
      });
    };
    return asyncOptionsFunc;
  };

  this.getAsyncOptionsFromDeferred = function(deferred) {
    var asyncOptionsFunc = function(input, callback) {
      deferred.done(function(data) {
        callback(null, {
          options: data,
          complete: true
        });
      });
    };
    return asyncOptionsFunc;
  };

  this.getTimesForDropdowns = function() {
    var times = [];
    for (var i = 0; i < 24; i++) {
      var s = i < 10 ? '0' + i : i;
      times.push({
        value: s + ':00:00',
        label: s + ':00'
      });
      times.push({
        value: s + ':15:00',
        label: s + ':15'
      });
      times.push({
        value: s + ':30:00',
        label: s + ':30'
      });
      times.push({
        value: s + ':45:00',
        label: s + ':45'
      });
    }
    return times;
  };

}

export default new mixins();
