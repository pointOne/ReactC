import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import HtmlPopup from 'ctl-htmlpopup';
import Mixins from 'comp';
import global from 'global';


class CrudControls extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showDelete: false
    };
  }

  componentWillUnmount() {
    this.unMounted = true;
  }

  render() {
    var o = this.props.dataObject;
    var OverlayTrigger = ReactBootstrap.OverlayTrigger;
    var Overlay = ReactBootstrap.Overlay;
    var ButtonGroup = ReactBootstrap.ButtonGroup;
    var Button = ReactBootstrap.Button;
    var buttonGroup, buttonEdit, buttonDelete;
    var deleteOverlay = null;

    if (o.More && o.More.ReadOnly) {
      buttonGroup = <span className="label label-default">read only</span>;
    } else {
      if (!o.More.PreventEdit) {
        buttonEdit = <Button bsStyle="default" bsSize="xsmall" onClick={() => {
          global.pub(this.props.entityNameCamelCase, 'grid-requested-edit', o.Id);
        }}>Edit</Button>;
      }

      if (!o.More.PreventRemove && !(o.More.InUse && o.More.InUse.length > 0)) {
        buttonDelete = <Button bsStyle="default" bsSize="xsmall" onClick={() => this.setState({
            showDelete: true
          })} ref="target">Remove</Button>;
        deleteOverlay = <Overlay rootClose={true} key={o.Id} show={this.state.showDelete}
        container={this} onHide={() => this.setState({
            showDelete: false
          })}>

                  <HtmlPopup style={{
          display: this.state.hideDelete ? 'none' : 'block',
          marginLeft: '-75px',
          marginTop: '-50px'
        }}>
                    <div>
                    <h4 style={{
          padding: '0px 0px 10px',

        }}>Are you sure?</h4><p>{this.props.deleteWarning}</p>
                      <ButtonGroup style={{
          padding: '2px'
        }}>
                        <Button bsStyle="success" bsSize="xsmall" onClick={() => {
          global.pub(this.props.entityNameCamelCase, 'removing', o.Id);
          this.setState({
            showDelete: false
          });
        }}>Yes</Button>
                          <Button bsStyle="default" bsSize="xsmall" onClick={() => this.setState({
            showDelete: false
          })}>No</Button>
                          </ButtonGroup>
                          </div>
                        </HtmlPopup>
                    </Overlay>;
      } else {
        if (o.More.InUse && o.More.InUse.length > 0) {
          buttonDelete = <OverlayTrigger trigger={["hover", "focus"]} placement="left" overlay={
          <HtmlPopup>{Mixins.topTenList(o.More.InUse, 'Where it\'s used')}</HtmlPopup>

          }><Button bsStyle="primary" bsSize="xsmall">In Use</Button>
                      </OverlayTrigger>;
        } else {
          buttonDelete = <Button bsStyle="warning" bsSize="xsmall">In Use</Button>;
        }
      }

      buttonGroup = <ButtonGroup className="crud-controls" onMouseOver={(e) => {
        //block bubbling to line item's handlers
        e.stopPropagation();
      }}>
                      {buttonEdit}
                      {buttonDelete}
                    </ButtonGroup>;
    }

    return (<div>{buttonGroup}{deleteOverlay}</div>);
  }
}

export default CrudControls;
