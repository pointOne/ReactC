import postal from 'bower/postal/lib/postal.min';

var global = {};

global.pub = function(channel, topic, data) {
  console.log('PUB: ' + channel + ' ' + topic, data);
  postal.publish({
    channel: channel,
    topic: topic,
    data: data
  });
};

global.sub = function(channel, topic, callback) {
  return postal.subscribe({
    channel: channel,
    topic: topic,
    callback: callback
  });
};

global.showLoader = function() {
  $('#overlay').show();
};

global.hideLoader = function() {
  $('#overlay').hide();
};

global.scrollUp = function() {
  $("html, body").animate({
    scrollTop: 0
  }, "fast");
};

global.localTime = function() {
  var tzoffset = (new Date()).getTimezoneOffset() * 60000;
  //offset in milliseconds
  var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1);
  return localISOTime;
};

export default global;
