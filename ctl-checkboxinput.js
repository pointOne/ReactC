import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import Formsy from 'formsy';
import global from 'global';
import store from 'store';

var CheckboxInput = React.createClass({

  mixins: [Formsy.Mixin],

  changeValue: function(event) {

    var newValue = !!event.target.checked;
    this.setValue(newValue);
    if (this.props.onChange) {
      this.props.onChange(newValue);
    }
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return (nextState._value !== this.state._value) ||
      (nextState._isRequired !== this.state._isRequired) ||
      (nextState._isValid !== this.state._isValid) ||
      (nextProps.disabled !== this.props.disabled);
  },

  render: function() {

    var className = this.isPristine() ? null : this.showRequired() ? 'error' : this.showError() ? 'error' : 'success';

    var Input = ReactBootstrap.Input;

    return (
      <div style={this.props.style}>
        <Input
      {...this.props}
      style={null}
      type = "checkbox"
      ref = "checkInput"
      checked = { this.getValue() }
      help = { this.getErrorMessage() }
      onChange = { this.changeValue }
      />
      </div>
      );
  }
});

export default CheckboxInput;
