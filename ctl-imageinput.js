import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import Formsy from 'formsy';
import reactAlert from 'ctl-reactalert';

// Converts canvas to an image
function convertCanvasToImage(canvas) {

  var image = new Image();
  image.src = canvas.toDataURL("image/png");
  return image;
}

// Converts image to canvas; returns new canvas element
function convertImageToCanvas(image) {

  var canvas = document.createElement("canvas");
  canvas.width = image.width;
  canvas.height = image.height;
  canvas.getContext("2d")
    .drawImage(image, 0, 0);
  return canvas;
}

function resize_canvas(srcCanvas, width, height) {

  var canvas, context;
  canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  context = canvas.getContext('2d');
  context.drawImage(srcCanvas, 0, 0, width, height);
  return canvas;
}

function merge_flatten(canvasWidth, canvasHeight, canvasToAppend) {

  var canvas, context;
  canvas = document.createElement('canvas');
  canvas.width = canvasWidth;
  canvas.height = canvasHeight;
  context = canvas.getContext('2d');
  var appendX = canvasToAppend.width < canvasWidth ? (canvasWidth - canvasToAppend.width) / 2 : 0;
  var appendY = canvasToAppend.height < canvasHeight ? (canvasHeight - canvasToAppend.height) / 2 : 0;
  context.drawImage(canvasToAppend, appendX, appendY);
  return canvas;
}

function scaleRound(num) {
  return Math.round(num * 100) / 100;
}

Math.round(1.49999 * 100) / 100

var Dropzone = React.createClass({

  getDefaultProps: function() {
    return {
      supportClick: true,
      multiple: false
    };
  },

  getInitialState: function() {
    return {
      isDragActive: false,
    }
  },

  propTypes: {
    onDrop: React.PropTypes.func.isRequired,
    size: React.PropTypes.number,
    style: React.PropTypes.object,
    supportClick: React.PropTypes.bool,
    accept: React.PropTypes.string,
    multiple: React.PropTypes.bool
  },

  onDragLeave: function(e) {
    this.setState({
      isDragActive: false
    });
  },

  onDragOver: function(e) {
    e.preventDefault();
    e.dataTransfer.dropEffect = "copy";

    this.setState({
      isDragActive: true
    });
  },

  onDrop: function(e) {
    e.preventDefault();

    this.setState({
      isDragActive: false
    });

    var files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }

    var maxFiles = (this.props.multiple) ? files.length : 1;
    for (var i = 0; i < maxFiles; i++) {
      files[i].preview = URL.createObjectURL(files[i]);
    }

    if (this.props.onDrop) {
      files = Array.prototype.slice.call(files, 0, maxFiles);
      this.props.onDrop(files, e);
    }
  },

  onClick: function() {
    if (this.props.supportClick === true) {
      this.open();
    }
  },

  open: function() {
    this.refs.fileInput.getDOMNode()
      .click();
  },

  render: function() {

    var className = this.props.className || 'dropzone';
    if (this.state.isDragActive) {
      className += ' active';
    }
    ;

    var style = {
      width: this.props.size || 100,
      height: this.props.size || 100,
      borderStyle: this.state.isDragActive ? "solid" : "dashed",
      borderWidth: "1px",
      padding: "10px",
      margin: "0px 20px 10px 0px",
      cursor: "pointer"
    };

    return (
    React.createElement("div", {
      className: className,
      style: style,
      onClick: this.onClick,
      onDragLeave: this.onDragLeave,
      onDragOver: this.onDragOver,
      onDrop: this.onDrop
    },
      React.createElement("input", {
        style: {
          display: 'none'
        },
        type: "file",
        multiple: this.props.multiple,
        ref: "fileInput",
        onChange: this.onDrop,
        accept: this.props.accept
      }),
      this.props.children
    )
    );
  }
});

var ImageInput = React.createClass({
  mixins: [Formsy.Mixin],

  shouldComponentUpdate: function(nextProps, nextState) {
    return (!_.isEqual(nextState._value, this.state._value)) ||
      (nextState._isRequired !== this.state._isRequired) ||
      (nextState._isValid !== this.state._isValid);
  },

  onDropSmall: function(files) {

    var orig = new Image();
    var _this = this;

    orig.onload = function() {
      _this.resizeImage(convertImageToCanvas(this), 'small');
    };

    orig.src = files[0].preview;
  },

  onDropMedium: function(files) {

    var orig = new Image();
    var _this = this;

    orig.onload = function() {
      _this.resizeImage(convertImageToCanvas(this), 'medium');
    };

    orig.src = files[0].preview;
  },

  onDropLarge: function(files) {

    var orig = new Image();
    var _this = this;

    orig.onload = function() {
      _this.resizeImage(convertImageToCanvas(this), 'large');
    };

    orig.src = files[0].preview;
  },

  resizeImage: function(canvas, size) {

    function resize(canvas, width, height) {

      var aspectRatio = scaleRound(canvas.width / canvas.height);

      if (aspectRatio === 1.5) {
        if (canvas.width === width && canvas.height === height) {
          // image is already the right size
          return canvas;
        } else if (canvas.width > width) {
          // image is right ratio but needs resizing
          return resize_canvas(canvas, width, height);
        } else {
          //image is too small for this size
          return null;
        }
      } else {
        if (canvas.width > width || canvas.height > height) {
          // we have data for this size but the ratio is wrong so we need to resize AND place on a background
          if (canvas.width > canvas.height) {
            var resized = resize_canvas(canvas, width, (width / canvas.width) * canvas.height);
          } else {
            var resized = resize_canvas(canvas, height, (height / canvas.height) * canvas.width);
          }
          return merge_flatten(width, height, resized);
        } else {
          //image is too small for this size
          return null;
        }
      }
    }

    var [Base64Small, Base64Medium, Base64Large] = this.getValue();

    if (size === 'small') {
      var small = resize(canvas, 100, 66);
      if (small) {
        Base64Small = convertCanvasToImage(small).src;
      } else reactAlert('The image you selected was too small. Please use an image whose dimensions are at least 100x66 pixels');
    }

    if (size === 'medium') {
      var medium = resize(canvas, 300, 200);
      if (medium) {
        Base64Medium = convertCanvasToImage(medium).src;

        if (!Base64Small) {
          var small = resize(medium, 100, 66);
          Base64Small = convertCanvasToImage(small).src;
        }
      } else reactAlert('The image you selected was too small. Please use an image whose dimensions are at least 300x200 pixels');
    }
    if (size === 'large') {
      var large = resize(canvas, 900, 600);

      if (large) {
        Base64Large = convertCanvasToImage(large).src;

        if (!Base64Medium) {
          var medium = resize(large, 300, 200);
          Base64Medium = convertCanvasToImage(medium).src;
        }

        if (!Base64Small) {
          var small = resize(medium, 100, 66);
          Base64Small = convertCanvasToImage(small).src;
        }
      } else reactAlert('The image you selected was too small. Please use an image whose dimensions are at least 900x600 pixels');
    }
    this.setValue([Base64Small, Base64Medium, Base64Large]);
  },

  render: function() {

    var Row = ReactBootstrap.Row;
    var Col = ReactBootstrap.Col;
    var Thumbnail = ReactBootstrap.Thumbnail;
    var Well = ReactBootstrap.Well;
    var [Base64Small, Base64Medium, Base64Large] = this.getValue() || [];

    return (
      <div>
		  <Row>
		    <Col>
		    <Well>
		      <Dropzone className="pull-left" onDrop={this.onDropSmall} size={100}>
		        <div>Drop file here, or click to select.</div>
		      </Dropzone>
		    	<h3>Small (100x33) </h3>
		        <p style={{
        marginTop: '-10px'
      }}>Used as an icon in lists</p>
  		     	 <img src={Base64Small} alt={ Base64Small ? '100x33' : "not uploaded" } />
		        </Well>

		        <Well>
                <Dropzone className="pull-left" onDrop={this.onDropMedium} size={100}>
		        <div>Drop file here, or click to select.</div>
		      </Dropzone>
		      <h3>Medium (300x200) </h3>
		        <p style={{
        marginTop: '-10px'
      }}>Used for menu item detail</p>
		      <img src={Base64Medium} alt={ Base64Medium ? '300x200' : "not uploaded" } />
		        </Well>
		        <Well>
                <Dropzone className="pull-left" onDrop={this.onDropLarge} size={100}>
		        <div>Drop file here, or click to select.</div>
		      </Dropzone>
		      <h3>Large (900x600) optional</h3>
		        <p style={{
        marginTop: '-10px'
      }}>Used for advertising</p>
		      <img src={Base64Large} alt={ Base64Large ? '900x600' : "not uploaded" } />
		      </Well>
		    </Col>
		  </Row>
		</div>
      );
  }
});

export default ImageInput;
