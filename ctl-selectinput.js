import React from 'react';
import Formsy from 'formsy';
import classNames from 'bower/classnames/index';
import ReactSelect from 'bower/react-select/lib/Select';

var SelectInput = React.createClass({

   mixins: [Formsy.Mixin],

   onChange: function(newValue) {
      let normalized = this.normalizeOnChange(newValue);
      this.setValue(normalized);
      if (this.props.onChange) {
         this.props.onChange(normalized);
      }
   },

   getInitialState: function() {
      return {
         id: (Math.random() + 1)
            .toString(36)
            .substring(7)
      };
   },

   shouldComponentUpdate: function(nextProps, nextState) {

      //settle for a comparison based on count here
      let prevCount = this.props.options ? this.props.options.length : 0;
      let nextCount = nextProps.options ? nextProps.options.length : 0;

      let shouldUpdate = (nextState._value !== this.state._value) ||
         (nextProps.value !== this.props.value) ||
         (nextState._isValid !== this.state._isValid) ||
         (prevCount !== nextCount) ||
         (nextProps.placeholder !== this.props.placeholder) ||
         (nextState._isRequired !== this.state._isRequired) ||
         (nextProps.disabled !== this.props.disabled) ||
         (nextProps.label !== this.props.label);
      return shouldUpdate;
   },

   componentDidMount: function() {
      $('.Select-control').addClass('form-control');
      if (this.props.listView === true) {
         $(`#${this.state.id}`).find('.Select-control').css('padding', '2px 5px 0px');
         $(`#${this.state.id}`).find('.Select-clear').hide();
         $(`#${this.state.id}`).find('.Select-arrow').hide();
         $(`#${this.state.id}`).find('.Select-item').css('display', 'block');
      }
   },

   componentDidUpdate: function(x, y) {
      if (this.props.listView === true) {
         $(`#${this.state.id}`).find('.Select-control').css('padding', '2px 5px 0px');
         $(`#${this.state.id}`).find('.Select-clear').hide();
         $(`#${this.state.id}`).find('.Select-arrow').hide();
         $(`#${this.state.id}`).find('.Select-item').css('display', 'block');
      }
   },

   splitOnComma: function(value) {
      var a = value.trim().split(',');
      a = _.filter(a, b => !!b || b === 0); //remove falsey values created by string split
      a = a.map(b => !isNaN(Number(b)) ? Number(b) : b); //convert numeric strings to numbers
      return a;
   },

   //this function is really only valuable on the initial value, on changing it normalizeOut() will clean it up
   normalizeInward: function(valueInward) {
      var valueInwardTransformed;

      if (this.props.multi === true) {
         if (valueInward === null || valueInward === undefined) {
            valueInwardTransformed = null;
         } else if (!_.isArray(valueInward)) {
            throw "Inbound value must be an array or null when multi is true";
         } else {
            valueInwardTransformed = valueInward.join(',');
         }
      } else if (valueInward === null || valueInward === undefined || valueInward === '00000000-0000-0000-0000-000000000000' || valueInward === "" || valueInward === 0) {
         valueInwardTransformed = null;
      } else {
         valueInwardTransformed = !isNaN(Number(valueInward)) ? Number(valueInward).toString() : valueInward.trim();
      }
      return valueInwardTransformed;
   },

   normalizeOnChange: function(valueOutward) {

      var valueOutwardTransformed;
      //we always start with a string
      //it could be empty string or null,  a single value, or a comma-delimited list of guids/ints
      if (this.props.multi === true) {
         if (valueOutward === null || valueOutward === "" || valueOutward === undefined) { //i don't think react-select can gen undefined but no harm in adding it
            valueOutwardTransformed = null;
         } else {
            valueOutwardTransformed = this.splitOnComma(valueOutward); //assume an array of numbers or guids or an empty string
         }
      } else {
         if (valueOutward === null || valueOutward === "" || valueOutward === undefined) { //i don't think react-select can gen undefined but no harm in adding it
            valueOutwardTransformed = null;
         } else if (!isNaN(Number(valueOutward))) {
            valueOutwardTransformed = Number(valueOutward); //assume a single number
         } else {
            valueOutwardTransformed = valueOutward; //assume a single guid
         }
      }
      return valueOutwardTransformed;
   },

   render: function() {

      var className = this.isPristine() ? null : this.showRequired() ? 'has-error' : this.showError() ? 'has-error' : 'has-success';
      var errorMessage = this.showRequired() ? "Required" : !this.isValid() ? (this.getErrorMessage() || "") : "";

      //if (this.props.name === "ReportingEntity") {
      // _onsole.log('name', this.props.name);
      // _onsole.log('this.props.value', this.props.value);
      // _onsole.log('this.state._isRequired', this.state._isRequired);
      // _onsole.log('this.state._isValid', this.state._isValid);
      // _onsole.log('pristine()', this.isPristine());
      // _onsole.log('isRequired()', this.isRequired());
      // _onsole.log('isValid()', this.isValid());
      // _onsole.log('showRequired()', this.showRequired());
      // _onsole.log('showError()', this.showError());
      // _onsole.log('errorMessage()', this.getErrorMessage());
      // _onsole.log('this.state._value', this.state._value);
      // _onsole.log(className);
      //}
      var placeholder = !this.props.disabled ? (this.props.multi ? "Select one or more..." : "Select one...") : "";

      return (
         <div className={classNames('form-group', className, {
            hide: this.props.hidden
         })} id = {this.state.id}>
        <label className="control-label">{this.props.label}</label>
        <ReactSelect
         {...this.props}
         required = {false}
         onChange = {this.onChange}
         onBlur = {this.onBlur}
         className = {className}
         value = {this.normalizeInward(this.getValue())}
         ref = 'ReactSelect'
         placeholder={placeholder}
         />
      <div className="help-block">{errorMessage}</div>
      </div>
         );
   }
});

export default SelectInput;
