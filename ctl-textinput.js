import autosize from 'bower/autosize/dist/autosize';
import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import Formsy from 'formsy';

var TextInput = React.createClass({
  mixins: [Formsy.Mixin],

  getDefaultProps: function() {
    return {
      rows: 1,
      disableCrLf: true,
      autoGrow: false
    };
  },

  getInitialState: function() {
    return {
      id: (Math.random() + 1)
        .toString(36)
        .substring(7)
    };
  },

  handleKeyDown: function(e) {
    if (this.props.autoGrow !== true && e.keyCode === 13) {
      e.preventDefault();
      e.stopPropagation();
    }
  },

  // setValue() will set the value of the component, which in turn will validate it and the rest of the form components as well, triggering re-renders along the way
  changeValue: function(event) {
    var newValue = event.target.value;
    this.setValue(newValue);
    if (this.props.onChange) {
      this.props.onChange(newValue);
    }
  },

  componentDidMount: function() {
    if (this.props.autoGrow === true) {
      autosize($('#' + this.state.id));
    }
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return (nextState._value !== this.state._value) ||
      (nextProps.value !== this.props.value) ||
      (nextState._isValid !== this.state._isValid) ||
      (nextProps.placeholder !== this.props.placeholder) ||
      (nextState._isRequired !== this.state._isRequired) ||
      (nextProps.disabled !== this.props.disabled) ||
      (nextProps.label !== this.props.label);
  },

  render: function() {

    // if (this.props.name === "ProductName") {
    // _onsole.log('name', this.props.name);
    // _onsole.log('this.props.value', this.props.value);
    // _onsole.log('this.state._isRequired', this.state._isRequired);
    // _onsole.log('this.state._isValid', this.state._isValid);
    // _onsole.log('this.state._value', this.state._value);
    // _onsole.log('pristine()', this.isPristine());
    // _onsole.log('isRequired()', this.isRequired());
    // _onsole.log('isValid()', this.isValid());
    // _onsole.log('showRequired()', this.showRequired());
    // _onsole.log('showError()', this.showError());
    // _onsole.log('errorMessage()', this.getErrorMessage());
    // }

    var errorMessage = this.showRequired() ? "Required" : !this.isValid() ? (this.getErrorMessage() || "") : "";

    var className = this.isPristine() ? null : this.showRequired() ? 'error' : this.showError() ? 'error' : 'success';

    var Input = ReactBootstrap.Input;

    var style;
    if (this.props.autoGrow === true) {
      style = {
        resize: 'none',
        whiteSpace: 'nowrap'
      };
    } else {
      style = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        resize: 'none'
      };
    }
    return (
      <Input {...this.props}
      required = {false} //.. avoid html5 val
      id = {this.state.id}
      type = "textarea"
      value = {this.getValue()}
      help = {errorMessage}
      bsStyle = {className}
      style = {style}
      onFocus = {this.onFocus}
      onKeyDown = {this.handleKeyDown}
      onChange = {this.changeValue}
      ref = 'textInput' />
      );
  }
});

export default TextInput;
