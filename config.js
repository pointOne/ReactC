System.config({
  "transpiler": "babel", //this only kicks in for classes it thinks are ESnext
  "babelOptions": {
    "blacklist": []
  },
  map: {
    "bower": "/bower_components", //for convenience
    "npm": "/node_modules", //for convenience
    "react": "/bower_components/react/react-with-addons", //this is for react-bootstraps webpack references
    "formsy": "/bower_components/formsy-react/src/main", //for convenience
    "react-bootstrap": "/bower_components/react-bootstrap/react-bootstrap", //for convenience
    'jquery': "/bower_components/jquery/dist/jquery.min", //jquery is loaded globally this is for imported modules that require jquery
    'datatables': "/bower_components/datatables/media/js/jquery.dataTables.min", //for datatables bootstrap integration
    'react-input-autosize': '/bower_components/react-input-autosize/lib/AutosizeInput', //for npm react-select
    'classnames': '/bower_components/classnames/index', // for npm react-select
    'react-router': '/bower_components/react-router/build/umd/ReactRouter', //for convenience
    'lodash': '/bower_components/lodash/lodash.min', //for postaljs
    '/bower_components/react-select/lib/SingleValue': '/js/reactselect-SingleValue', //override
    '/bower_components/react-select/lib/Value': '/js/reactselect-Value' //override
  },
  paths: {
    '/bower_components/formsy-react/src/validationRules.js': '/bower_components/formsy-react/src/validationRules.js', //for formsy src
    '/bower_components/formsy-react/src/utils.js': '/js/formsy-utils.js', //override
    '/bower_components/formsy-react/src/Mixin.js': '/bower_components/formsy-react/src/Mixin.js', //for formsy src

  }
});
