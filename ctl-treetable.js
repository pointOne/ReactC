/* Because on the server-side implementation the
same node can belong to multiple parents or because
deleting a node does not delete but re-arranges it's
children we have two choices either build all those
rules into the client-side tree which might be different
depending on where it is used so they would need to be options.
Or re-draw the entire tree from a new dataset retrieved
from the server. At this time I have chosen the latter.
*/
import store from 'store';
import React from 'react';
import CrudControls from 'ctl-crudgridcontrols';
import * as ReactBootstrap from 'react-bootstrap';
import ReactPersistentState from 'mix-ctlstate';
import global from 'global';

var TableRow = React.createClass({

  propTypes: {
    //copied from parent/treetable
    nodeStyle: React.PropTypes.shape({
      color: React.PropTypes.string,
      backColor: React.PropTypes.string,
      selectedColor: React.PropTypes.string,
      selectedBackColor: React.PropTypes.string,
      borderColor: React.PropTypes.string,
      enableLinks: React.PropTypes.bool,
      highlightSelected: React.PropTypes.bool,
      showBorder: React.PropTypes.bool,
    }),

    selectableRows: React.PropTypes.array,
    selectedRows: React.PropTypes.array,
    nodeHrefField: React.PropTypes.string,
    nodeTextField: React.PropTypes.string.isRequired,
    //unique to node
    node: React.PropTypes.object.isRequired,
    rowSelected: React.PropTypes.func.isRequired,
    rowDeSelected: React.PropTypes.func.isRequired,
    rowExpanded: React.PropTypes.func.isRequired,
    rowCollapsed: React.PropTypes.func.isRequired,
    expandedRows: React.PropTypes.array.isRequired,
    entityNameCamelCase: React.PropTypes.string,
    entityNamePascalCase: React.PropTypes.string,
    rowDetailToggled: React.PropTypes.func,
    detailViewParentRowId: React.PropTypes.string,
    detailViewData: React.PropTypes.array,
    persistentStateKey: React.PropTypes.string
  },

  getInitialState: function() {
    return {
      hovering: false
    };
  },

  render: function() {

    var node = this.props.node;
    var styleOptions = this.props.nodeStyle;

    // BEGIN STYLING
    var style;
    if (node.parent && !_.some(this.props.expandedRows, (i) => i === node.parent.Id)) {
      style = {
        display: 'none'
      };
    } else {
      if ((styleOptions.highlightSelected && _.some(this.props.selectedRows, {
          Id: node.Id
        })) ||
        this.state.hovering === true) {
        style = {
          color: styleOptions.selectedColor,
          backgroundColor: styleOptions.selectedBackColor
        };
      } else {
        style = {
          color: styleOptions.color,
          backgroundColor: styleOptions.backColor
        };
      }
      if (!styleOptions.showBorder) {
        style.border = 'none';
      } else if (styleOptions.borderColor) {
        style.border = '1px solid ' + styleOptions.borderColor;
      }
    }

    var indents = [];
    for (var i = 0; i < node.level - 1; i++) {
      indents.push(<span className="indent" key={i}></span>);
    }

    var expandCollapseIcon = null;
    if (node.nodes && node.nodes.length > 0) {
      expandCollapseIcon = <a onClick= {_.includes(this.props.expandedRows, node.Id) ? () => this.props.rowCollapsed(node.Id) : () => this.props.rowExpanded(node.Id)}>
        <i className={_.includes(this.props.expandedRows, node.Id) ? "fa fa-minus-square" : "fa fa-plus-square" } /></a>;
    }

    var nodeText = null;
    if (styleOptions.enableLinks && this.props.nodeHrefField) {
      nodeText = <a href={node[this.props.nodeHrefField]}>
        {node[this.props.nodeTextField]}
      </a>;
    } else {
      nodeText = <span>{node[this.props.nodeTextField]}</span>;
    }

    var detailViewer = null;
    if (this.props.children) {
      detailViewer = <ReactBootstrap.OverlayTrigger placement='right' delayShow={1500} overlay={
      <div style={{
        position: "absolute",
        zIndex: 100,
        backgroundColor: 'white',
        boxShadow: '0 5px 10px rgba(0, 0, 0, 0.2)',
        border: '1px solid #CCC',
        borderRadius: 2,
        padding: 3,
        fontSize: '0.99rem'
      }}>{this.props.detailToolTip}</div>}><a onClick={() => {
        this.props.rowDetailToggled(node.Id);
      }}>
      <i
      className="fa fa-th"
      /></a>
      </ReactBootstrap.OverlayTrigger>;
    }

    var detail = null;
    if (this.props.detailViewParentRowId === node.Id) {
      let newProps = {
        data: this.props.detailViewData
      };
      if (newProps.data && newProps.data.length) {
        detail = <div>
          {React.Children.map(this.props.children, (child) => React.cloneElement(child, newProps))}
        </div>;
      } else {
        detail = <span style={{
          display: 'inline-block',
          marginTop: 5
        }}>Nothing to display</span>;
      }
    }

    var tickbox = null;
    if (_.includes(this.props.selectableRows, node.Id)) {
      tickbox = <i
      className = {_.some(this.props.selectedRows, (i) => i === node.Id) ? "fa fa-check-square" : "fa fa-square-o"}
      onClick={_.some(this.props.selectedRows, (i) => i === node.Id) ? () => this.props.rowDeSelected(node.Id) : () => this.props.rowSelected(node.Id)}
      />;
    }

    var controls = this.props.showControls ? <CrudControls
    dataObject = {node}
    entityNamePascalCase = {this.props.entityNamePascalCase}
    entityNameCamelCase = {this.props.entityNameCamelCase}
    /> : null;

    return (
      <li
      className="list-group-item noselect"
      style={style}
      onMouseOver={() => this.setState({
          hovering: true
        })}
      onMouseOut={() => this.setState({
          hovering: false
        })}
      >
        <div>
          <div className="title-row">
            <div>
              {indents}
              {expandCollapseIcon}
              {detailViewer}
              {tickbox}
              {nodeText}
            </div>
            <div>
              {controls}
            </div>
          </div>
          <div className="detail-row">
              {indents}
              {detail}
          </div>
      </div>
      </li>);
  }
});

function forEachRecursive(nodes, fForEach) {
  if (nodes) {
    nodes.forEach(function(node) {
      fForEach(node);
      forEachRecursive(node.nodes, fForEach);
    });
  }
}

var TreeTable = React.createClass({

  mixins: [ReactPersistentState],

  propTypes: {
    nodeStyle: React.PropTypes.shape({
      color: React.PropTypes.string,
      backColor: React.PropTypes.string,
      borderColor: React.PropTypes.string,
      selectedColor: React.PropTypes.string,
      selectedBackColor: React.PropTypes.string,
      enableLinks: React.PropTypes.bool,
      highlightSelected: React.PropTypes.bool,
      showBorder: React.PropTypes.bool,
    }),
    selectableNodes: React.PropTypes.array,
    selectedNodes: React.PropTypes.array,
    isSelectionExclusive: React.PropTypes.bool,
    nodeHrefField: React.PropTypes.string,
    nodeTextField: React.PropTypes.string.isRequired,
    showControls: React.PropTypes.bool,
    entityNameCamelCase: React.PropTypes.string,
    entityNamePascalCase: React.PropTypes.string,
    detailEntityNamePascalCase: React.PropTypes.string,
    detailEntityFilterPredicate: React.PropTypes.func,
    data: React.PropTypes.arrayOf(React.PropTypes.object),
  },

  getDefaultProps: function() {
    return {
      nodeStyle: {
        color: undefined,
        backColor: undefined,
        borderColor: undefined,
        selectedColor: 'black',
        selectedBackColor: '#f5f5f5',
        enableLinks: false,
        highlightSelected: true,
        isSelectionExclusive: false,
        showBorder: true,
      },
      data: []
    };
  },

  getInitialState: function() {
    return {
      selectedNodes: this.props.selectedNodes || [],
      expandedNodes: [],
      subs: []
    };
  },

  subs: [],

  componentWillMount: function() {
    this.subs.push(global.sub(this.props.entityNameCamelCase, "removing", (data, env) => {
      store['remove' + this.props.entityNamePascalCase](data).done(() => {
        global.pub(this.props.entityNameCamelCase, 'removed', data);
      });
    }));
  },

  componentWillUnmount: function() {
    $.each(this.subs, function(index, value) {
      value.unsubscribe();
    });
  },

  componentDidMount: function() {
    if (this.props.persistentStateKey) {
      this.setPId('treetable-state-' + this.props.persistentStateKey);
      this.setPStorage(this.localStorage);
      this.restorePState();
    }
  },

  componentWillUpdate: function(nP, nS) {
    if (this.props.persistentStateKey) {
      this.setPState(nS);
    }
  },

  rowSelected: function(id) {
    let selectedNodes = this.state.selectedNodes;
    if (this.props.isSelectionExclusive) {
      this.setState({
        selectedNodes: [id]
      });
    } else {
      this.setState({
        selectedNodes: _.union(selectedNodes, [id])
      });
    }
  },
  rowDeSelected: function(id) {
    let selectedNodes = this.state.selectedNodes;
    if (this.props.isSelectionExclusive) {
      this.setState({
        selectedNodes: []
      });
    } else {
      this.setState({
        selectedNodes: _.filter(selectedNodes, (nodeId) => nodeId !== id)
      });
    }
  },
  rowExpanded: function(id) {
    let expandedNodes = this.state.expandedNodes;
    this.setState({
      expandedNodes: _.union(expandedNodes, [id])
    });
  },
  rowCollapsed: function(id) {
    let expandedNodes = this.state.expandedNodes;
    this.setState({
      expandedNodes: _.filter(expandedNodes, (nodeId) => nodeId !== id)
    });
  },

  getDetailData: function() {
    var deferred = $.Deferred();
    if (this.props.detailData) {
      deferred.resolve(this.props.detailData);
    } else {
      store[`load${this.props.detailEntityNamePascalCase}Collection`]().done((data) => deferred.resolve(data));
    }
    return deferred;
  },

  rowDetailToggled: function(id) {

    this.getDetailData().done((data) => {
      let ctx = {
        parentRowId: id
      };
      let filteredData = _.filter(data, this.props.detailEntityFilterPredicate, ctx);

      this.setState({
        detailViewParentRowId: this.state.detailViewParentRowId === id ? null : id,
        detailViewData: this.state.detailViewParentRowId === id ? null : filteredData
      });
    });
  },

  render: function() {



    var transferProps = _.clone(this.props);
    delete transferProps.data;
    delete transferProps.isSelectionExclusive;
    delete transferProps.selectedNodes;
    delete transferProps.selectableNodes;

    let rows = [];
    forEachRecursive(this.props.data, (node) => {
      rows.push(<TableRow
      {...transferProps}
      key={node.Id + "-" + (node.parent ? node.parent.Id : "")}
      node = {node}
      rowSelected = {this.rowSelected}
      rowDeSelected = {this.rowDeSelected}
      rowExpanded = {this.rowExpanded}
      rowCollapsed = {this.rowCollapsed}
      selectedRows = {this.state.selectedNodes}
      selectableRows = {this.props.selectableNodes}
      expandedRows = {this.state.expandedNodes}
      rowDetailToggled = {this.rowDetailToggled}
      detailViewParentRowId = {this.state.detailViewParentRowId}
      detailViewData = {this.state.detailViewData}
      />);
    });
    return (<div className="treeview">
        <ul className="list-group">
          {rows}
        </ul>
      </div>);
  }
});

export default TreeTable;
