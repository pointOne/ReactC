var React = require('react');

var HtmlPopup = React.createClass({

  render: function() {

    var style = {
      backgroundColor: 'white',
      boxShadow: '0 5px 10px rgba(0, 0, 0, 0.2)',
      border: '1px solid #CCC',
      borderRadius: 3,
      padding: 10,
      position: 'absolute',
      zIndex: 100
    };


    return (
    React.createElement("div", {
      style: _.assign(style, this.props.style)
    }, this.props.children)
    );
  }
});

module.exports = HtmlPopup;
