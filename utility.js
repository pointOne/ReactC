import 'lodash';

//lodash utility extensions
_.mixin({
  swap: function(myArr, predicateObj, replacement) {
    return _.map(myArr, function(el) {
      var property = Object.keys(predicateObj)[0];
      return el[property] === predicateObj[property] ? replacement : el;
    });
  }
});


_.mixin({
  isGuid: function(value) {
    var validGuid = /^({|()?[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}(}|))?$/;
    var emptyGuid = /^({|()?0{8}-(0{4}-){3}0{12}(}|))?$/;
    var validInt = /^\d+$/;
    return (validGuid.test(value) && !emptyGuid.test(value)) || validInt.test(value);
  }
});
