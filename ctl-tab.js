import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';

function getNavStates(indx, length) {
  var styles = [];
  for (var i = 0; i < length; i++) {
    if (i === indx) {
      styles.push('current');
    } else {
      styles.push('normal');
    }
  }
  return {
    current: indx,
    styles: styles
  };

}

var Tab = React.createClass({
  render: function() {
    return (
      <div>
          {this.props.children}
      </div>
      );
  }
});

export { Tab };

var Tabs = React.createClass({


  getInitialState: function getInitialState() {
    return {
      compState: 0,
      navState: getNavStates(0, React.Children.count(this.props.children))
    };
  },

  setNavState: function setNavState(next) {

    this.setState({
      navState: getNavStates(next, React.Children.count(this.props.children)),
      compState: next
    });

  },

  handleOnClick: function handleOnClick(evt) {

    var stepNo = (event.target.value === undefined ? $(event.target).parent().get(0).value : event.target.value);
    this.setNavState(stepNo);
    if (this.props.onTabClick) {
      this.props.onTabClick(stepNo);
    }

  },

  render: function render() {

    var _this = this;

    return <div className="multistep" onKeyDown={this.handleKeyDown}>
          <ol className="progtrckr text-center">
            {
      React.Children.map(this.props.children, function(s, i) {
        if (s === null) {
          return null;
        }
        return <li value={i} key={i}
          className={"noselect progtrckr-" + _this.state.navState.styles[i]}
          onClick={_this.handleOnClick}>
                  <em>{i + 1}</em>
                  <span>{s.props.title}</span>
                </li>;
      })
      }
          </ol>
          {
      React.Children.map(this.props.children, function(s, i) {

        return (<div style={{
            display: (_this.state.compState === i ? "block" : "none")
          }}>
                    {s}
                </div>
          );
      })
      }
        </div>;
  }
});

export { Tabs };
